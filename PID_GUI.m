function varargout = PID_GUI(varargin)
% PID_GUI MATLAB code for PID_GUI.fig
%      PID_GUI, by itself, creates a new PID_GUI or raises the existing
%      singleton*.
%
%      H = PID_GUI returns the handle to a new PID_GUI or the handle to
%      the existing singleton*.
%
%      PID_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PID_GUI.M with the given input arguments.
%
%      PID_GUI('Property','Value',...) creates a new PID_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PID_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PID_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PID_GUI

% Last Modified by GUIDE v2.5 18-Aug-2015 23:19:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PID_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @PID_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%ha=axes('units','normalized','position',[0 0 1 1]); 
%uistack(ha,'down');
%II=imread('1.jpg'); 
%image(II) 
%colormap gray 
%set(ha,'handlevisibility','off','visible','off'); 

% --- Executes just before PID_GUI is made visible.
function PID_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PID_GUI (see VARARGIN)

% Choose default command line output for PID_GUI
handles.output = hObject;

axes(handles.axes1);
II=importdata('1.jpg'); 
image(II);

axes(handles.axes2);
III=importdata('2.jpg'); 
image(III);
%axis off
% Update handles structure
guidata(hObject, handles);



% UIWAIT makes PID_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PID_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
    input = str2num(get(hObject,'String')); %以字符串的形式来存储数据文本框1的内容
	if  (isempty(input))     
	  set(hObject,'String','0'); %检查输入是否为空. 如果为空或是输入非数字字符,则默认显示为0
	end
	guidata(hObject, handles); 

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
    input = str2num(get(hObject,'String')); %以字符串的形式来存储数据文本框1的内容
	if  (isempty(input))     
	  set(hObject,'String','0'); %检查输入是否为空. 如果为空或是输入非数字字符,则默认显示为0
	end
	guidata(hObject, handles); 

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num0=str2num(get(handles.edit1,'String'));
den0=str2num(get(handles.edit2,'String'));
sys2=tf(num0,den0);                %已知传递函数
zeter=0.01:0.01:1;                 %比例度
n=length(zeter);                   %Kp个数，即选择范围
for i=1:n
   sys1_P(i)=tf(1/zeter(i));       %PID部分传递函数
   %寻找比例系数
   [Zp(i),sjb_P(i),Ts_P(i),JingCha(i),K_P(i),y_m(i),TTr_P(i)]=P_tune(sys1_P(i),zeter(i),num0,den0);
end
%求Zp中非0个数
lZp=length(Zp);
lZp0=find(Zp~=0); 
lZp_f0=length(lZp0);                                     %Zp中非0个数

%根据Zp画出响应曲线，计算上升时间，调节时间   
for i=1:lZp
    set(handles.text12,'String','');             %初始化
    set(handles.text13,'String','');             %初始化
    set(handles.text14,'String','');             %初始化
 if Zp(i)==0
     continue;
 else
       set(handles.text12,'String',Zp(i));                   %显示整定结果
       [TTr(i),t,y] = disp_P(Zp(i),num0,den0);  
       axes(handles.axes2)
       plot(t,y)
       set(handles.axes2,'XminorTick','on')
       grid on
       set(handles.text10,'String',TTr_P(i));                   %显示整定结果
       set(handles.text11,'String',Ts_P(i));                    %显示整定结果 
   %判断是否满足要求
   PD = questdlg('满足要求吗？','提示','Yes','No','Yes');
   PanDuan(i)=strcmpi(PD,'Yes');
   if PanDuan(i)==1                                         %如果比例作用满足要求
       msgbox('整定完成！')                                  %整定完成，结束程序
       close(handle);
       else  if  PanDuan(i)==0                              %仅比例作用不满足要求，加入积分作用
       ZeTer_I(i)=1.2*Zp(i);                                %根据衰减曲线法，将比例度变为原来的1.2倍
       Ti(i)=0.5*TTr(i);                                    %选择积分时间
       sys1_PI(i)=tf([(1/ZeTer_I(i))*Ti,1/ZeTer_I(i)],[Ti(i) 0]);%PID部分传递函数
       [num_PI(i),den_PI(i)]=GouZaotf(sys1_PI(i),num0,den0);%待整定传递函数系数
       %稳定性判断,调用WenDingXing函数，返回稳定性判断标志k,k=0--系统不稳定，k=1--系统稳定
       K_PI(i)=WenDingXing(num_PI(i),den_PI(i));
       %调用DongTaiZhiBiao函数，返回上升时间Tr、调节时间Ts、衰减比sjb和静差
       [Tr_PI(i),Ts_PI(i),sjb_PI(i),~,JingCha_PI(i)]=DongTaiZhiBiao(num_PI(i),den_PI(i));
       %调用输出函数，输出性能指标并画出单位阶跃响应曲线
       if K_PI(i)==1 && Ts_PI(i)~=0                         %判断系统是否稳定
       
       Eva_PI(i)=Tr_PI(i)+Ts_PI(i);                         %评价指标，上升时间和调节时间最小
       else
           continue;                       
       end
       end
   end
 end
end
       %判断是否满足要求
       Eva_PI(Eva_PI==0)=inf;
       [~,l_I]=min(Eva_PI); 
       sys1_PI(i)=tf([(1/ZeTer_I(l_I))*Ti,1/ZeTer_I(l_I)],[Ti(l_I) 0]);%PID部分传递函数
       [num_PI(i),den_PI(i)]=GouZaotf(sys1_PI(i),num0,den0);%待整定传递函数系数
       %调用DongTaiZhiBiao函数，返回上升时间Tr、调节时间Ts、衰减比sjb和静差
       [Tr_PI(i),Ts_PI(i),sjb_PI(i),~,JingCha_PI(i)]=DongTaiZhiBiao(num_PI(i),den_PI(i));
       set(handles.text12,'String',ZeTer_I(l_I));        %显示整定结果
       set(handles.text13,'String',Ti(l_I));             %显示整定结果 
       [y,t]=disp_PI(num_PI(i),den_PI(i));
       axes(handles.axes2)
       plot(t,y)
       set(handles.axes2,'XminorTick','on')
       grid on
       set(handles.text10,'String',Tr_PI(i));               %显示整定结果
       set(handles.text11,'String',Ts_PI(i));               %显示整定结果 
       PD_PI = questdlg('满足要求吗？','提示','Yes','No','Yes');
       PanDuan_PI(i)=strcmpi(PD_PI,'Yes');
         if PanDuan_PI(i)==1                                     %如果比例作用满足要求
          msgbox('整定完成！')                                 %整定完成，结束程序
          close(handle);
         else if  PanDuan_PI(i)==0                              %加入微分作用
                 for i=1:lZp
                     if Zp(i)==0
                     continue;
                     else
                    %整定PID参数
                    [Eva_PID(i),ZeTer_PID(i),I_PID(i),D_PID(i),Tr_PID(i),Ts_PID(i)] = PID_tune(Zp(i),TTr(i),num0,den0);
                    end
                end
             end
         end
    %显示整定结果
       Eva_PID(Eva_PID==0)=inf;
       [~,l_PID]=min(Eva_PID); 
       set(handles.text12,'String',ZeTer_PID(l_PID));        %显示整定结果
       set(handles.text13,'String',I_PID(l_PID));                %显示整定结果 
       set(handles.text14,'String',D_PID(l_PID));                %显示整定结果 
       K2=(1/ZeTer_PID(l_PID)*I_PID(l_PID)*D_PID(l_PID));
       K1=(1/ZeTer_PID(l_PID))*I_PID(l_PID);
       K0=(1/ZeTer_PID(l_PID));
       sys1_PID=tf([K2,K1,K0],[I_PID(l_PID),0]);
       [num_PID,den_PID]=GouZaotf(sys1_PID,num0,den0);           %待整定传递函数系数
       t=0:0.01:30;
       sys=tf(num_PID,den_PID);
       y=step(sys,t);
       axes(handles.axes2)
       plot(t,y)
       set(handles.axes2,'XminorTick','on')
       grid on
       set(handles.text10,'String',Tr_PID(l_PID));               %显示整定结果
       set(handles.text11,'String',Ts_PID(l_PID));               %显示整定结果 
       msgbox('整定完成！')                                 %整定完成，结束程序
       close(handle);
guidata(hObject, handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
    input = get(hObject,'String'); %以字符串的形式来存储文本
	if  (isempty(input))     
	  set(hObject,'String','0'); %检查输入是否为空. 如果为空或是输入非数字字符,则默认显示为0
	end
	guidata(hObject, handles); 

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over edit8.
function edit8_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'string',' ');
set(handles.edit2,'string',' ');
set(handles.text10,'string',' ');
set(handles.text11,'string',' ');
set(handles.text12,'string',' ');
set(handles.text13,'string',' ');
set(handles.text14,'string',' ');
cla;


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2
